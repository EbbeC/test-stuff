/**
 * @todo: refactor jQuery out to maximise performance. Create addClass, removeClass and hasClass functions

 NOTE: I am making an assumption that we are expecting the user
 to have a browser which supports classList.
 */

/*
*************************************************************
* I did this one because I read the task wrong.
* I first thought we wanted to get rid of jQuery. 
* Let me try that again below
*************************************************************
* /

(function() {

    function addClass(el, cls) {
        if ( el.length ) {
            el = Array.prototype.slice.call(el);
            el.map(function(item) {
                item.classList.add(cls);
            });           
        } else {
            el.classList.add(cls);
        }
    }

    function doTheStuff(e) {
        e.preventDefault();
        var item = this;
        var submenu = item.querySelectorAll('ul');

        if ( submenu.length > 0 ) {
            addClass(submenu, 'open');
            return;
        } else {
            var link = item.querySelector('a');
            window.location = link.href;
        }
    }

    var menuLi = document.querySelector('#menu li');
    menuLi.addEventListener('click', doTheStuff);
    menuLi.addEventListener('touchstart', doTheStuff);

})();

/*
*************************************************************
* Ok. Let me try that again. But still going to 
* disregard jQuery..
*************************************************************
*/
// Let's build the goodies
var myLibrary = {
    on: function(ev, fun) {
        var events = ev.split(' ');
        if ( this.element ) {
            objToArr(this.element).map(function(item) {
                events.map(function(ev) {
                    item.addEventListener(ev, fun);
                });
            });
        }
        return this;
    },
    find: function(needle) {
        if ( this.element ) {
            return new eQuery(this.element.querySelectorAll(needle));
        }
    },
    addClass: function(cls) {
        if ( this.element ) {
            objToArr(this.element).map(function(item) {
                item.classList.add(cls);
            });
        }
        return this;
    },
    // Only expecting one element to be sent here.
    // For consistency keeping everything as lists anyway.
    attr: function(attr) {
        if ( this.element && attr ) {
            return this.element[0][attr];
        }
    }
};

// Lets make a DOM wrapper
function eQuery(el) {
    this.element = el;
    this.length = el.length;
}

// And fill it with the goodies
Object.keys(myLibrary).map(function(key, index) {
    eQuery.prototype[key] = myLibrary[key];
});

// A little helper
function objToArr(obj) {
    return Array.prototype.slice.call(obj);
}

// eQuery enters the stage.
function $(el) {
    if ( typeof el === 'string' ) {
        el = document.querySelectorAll(el);
    } else if ( typeof el === 'object') {
        // Maybe I'll use this some day.
    } else {
        console.alert('Incorrect parameter for $');
    }

    return new eQuery(el);
}

// Aaaaand, now let's see if the code works.
$('#menu li').on('touchstart click', function (e) {

    e.preventDefault();
    
    var item = $(this);
    var submenu = item.find('ul');

    // Display submenu, if there is one
    // Otherwise follow the link
    if ( submenu.length > 0 ) {
        submenu.addClass('open');
        return;
    } else {
        var link = item.find('a');
        window.location = link.attr('href');
    }

});
