/**
 *
 * @todo Implement methods for manipulating arrays and create usage examples per function with provided arrays bonusMoneyIds and freeSpinIds
 *
 */
var Utils = {};

/**
 * Add item to array
 */
Utils.add = function(arr, item) {
	arr.push(item);
	return arr;
};

/**
 * Update array value based on index
 */
Utils.update = function(arr, index, val) {
	arr[index] = val;
	return arr;
};

/**
* Remove array item based on index
* */
Utils.remove = function(arr, index) {
	arr.splice(index, 1);
	return arr;
};

/**
 * Combine two arrays to one
 */
Utils.combine = function(arr1, arr2) {
	return arr1.concat(arr2);
};

/**
 * Get array item position based on value
 */
Utils.getPosition = function(arr, val) {
	for ( var i=0; i<arr.length; i++ ) {
		if ( arr[i] === val ) return i;
	}
};

/**
 * Sort array of integers in ascending order
 */
Utils.sortAscending = function(arr) {
	return arr.sort(function(a, b) {
		return a - b;
	});
};

/**
 * Sort array of integers in descending order
 */
Utils.sortDescending = function(arr) {
	return arr.sort(function(a, b) {
		return b - a;
	});
};

var bonusMoneyIds = [2,6,3,10,5,1,4,11];
var freeSpinIds = [15,14,12,13];

var boop = Utils.add(bonusMoneyIds, 11112);
console.log(boop);
var boop = Utils.update(bonusMoneyIds, 2, 1313);
console.log(boop);
var boop = Utils.remove(bonusMoneyIds, 2);
console.log(boop);
var boop = Utils.combine(bonusMoneyIds, freeSpinIds);
console.log(boop);
var boop = Utils.getPosition(bonusMoneyIds, 11112);
console.log(boop);
var boop = Utils.sortAscending(bonusMoneyIds);
console.log(boop);
var boop = Utils.sortDescending(bonusMoneyIds);
console.log(boop);

/**
 * @todo: Utils usage examples
 */
