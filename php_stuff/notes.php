<?php 
/*
*************************************************************
* Test 1
*************************************************************
* /
- Create class User
- 4 different types of items can be assigned to user (in future more)

1. design the SQL for saving items
2. design API to add and remove items from users
3. implement function exchange() in PHP which exchanges 5 free spins to 5 euros or vice versa
4. How would you utilize Redis for caching?

Check this out at php_test.php file.

/*
*************************************************************
* Test 2
*************************************************************
* /

class User {
	$id;  // stored to session // <-- NOTE #1: Are we a protected, private or public property? Or even static?
	public funtion getWheel($wheelId) { // <-- NOTE #2: funtions work less reliably than functions :D
		$sql = "SELECT * FROM User_wheels where wheelId = $wheelId"; // <-- NOTE #3: This is an invite for injection.
		$result = $pdo->query($sql)->fetch(); // NOTE #4: I'd be more comfortable with prepared statements.
		return $result;
	}
} // <-- NOTE #4: Maybe not end the class just yet.

public function getUser() {
	if ($loggedIn) return null; // NOTE #5: Where is this $loggedIn defined. We probably need to dig deeper.
	else return $session->getUser(); // NOTE #6: Where is this $session defined. We probably need to dig deeper. Also...
}

public function rewardWheelToUser($user, $wheel) { 
	// <-- NOTE #7: I would check whether the stuff we are expecting exists before playing around with the DB.
	$sql = "DELETE * FROM User_wheels WHERE wheelId = $wheel->id"; // NOTE #8: I like to use `back_ticks`.
	$result = $this->db->query($sql); // NOTE #9: Where is this $this->db you speak of?

	$sql ="INSERT INTO User_rewards (userId, reward) values ($user->id, $wheel->reward)"; // NOTE #10: Hoping that all these are integers.
	$result = $this->db->query($sql); // NOTE #11: So we are overwriting the previous result?

	return $result; // NOTE #12: Maybe we should check each query separately and provide a success if both succeed. Or separete this into 2 functions. 
}

public function spinAction($wheelId)
{ // <-- NOTE #13: Make up your mind! 
	$user = getUser(); // NOTE #14: I'm assuming this should be part of the User class, so $this-> could help.
	$wheel = $user->getWheel($wheelId); // NOTE #15: Many issues here, read notes of notes.
	$success = rewardWheelToUser($user, $wheel); // NOTE #16: $this-> and also maybe just do this if $user and $wheel were fetched succesfully.

	return json(['success' => $success]); // NOTE #17: Try json_encode()
}

// Most of my answers are relying on the assumption that we are supposed to close the class here (and maybe not starting other ones in the middle there somewhere).

/*
*************************************************************
* Notes on the notes
*************************************************************
*/
#3: I would use prepared statements and even then I would validate the input just in case (even though I would normally validate it before it hits this method). If the $wheelId is expected to be an integer maybe cast it to int first.
#4: So here I made the assumption that we are not really supposed to end the class yet. Otherwise the "public" words in front of all the rest of the functions don't really make sense. So I am going to assume that all of these functions are in fact methods of the same class.
#6: Seems weird to return null for a logged in user and try to fetch the user if they are logged out..
#7: Maybe just validate that the properties exist and look like how we expect them to before adding them in a query.
#8: In my experience using back ticks for column names and database names really help to avoid these weird issues of queries just not working for some weird reason.
#12: I would probably do both of the calls as separate methods and only return a positive result if both pass. I would probably wrap both calls in a try/catch and return the caught error if one of them fails.
#13: No.
#14: Considering that I don't see $session defined or this as extended class, this is probably a bad idea. Even with extended class I would expect $this->session->getUser(); and there I'm trusting that the parent has such a thing going on.
#15: Seems that $user is supposed to be a (undefined and uninitialised) session instance not an instance of this class. Even then we would use $this->. Also if the $wheelId is coming from the uri we definitely want to sanitise it before passing it here.
?>