<?php
class DB {
	private $connection;
	private static $instance;

	private function __construct() {
		$this->connection = new mysqli('127.0.0.1', 'root', 'root', 'rizk', 8889);

		if ($this->connection->connect_error) {
			echo $this->connection->connect_error;die();
		}
	}

	public static function Create() {
		if (!isset(self::$instance))
		{
			self::$instance = new DB;
		}

		return self::$instance;		
	}

	/**
	 * Old school dirty query (only for selections)
	 * @param string 		$sql 		The query string
	 * @return array|boolean 			The result array or false
	 */
	public function Query($sql) {

		$qry = $this->connection->query($sql);

		if ( $qry ) {
			$arr = array();
			while ( $rs = $qry->fetch_assoc() ) $arr[] = $rs;

			return $arr;
		}
	}
	/**
	 * Old school dirty query (for other than select)
	 * @param string 		$sql 		The query string
	 * @return boolean		 			Did we succeed,
	 */
	public function Value($sql) {
		return $this->connection->query($sql);
	}
	/**
	 * Prepared statement helper to add values to table.
	 * @param string 	$table 		Which table we target
	 * @param array 	$column 	Which columns (in order)
	 * @param array 	$value 		What values (in order)
	 * @param string 	$typer 		Type of $where values in order. "i" or "s". Ex. "siss"
	 * @return boolean				Did we succeed.
	 */
	public function InsertValue($table, $columns, $values, $types) {
		$questionMarks 	= '';
		foreach ($columns as $column) $questionMarks .= '?, ';
		$questionMarks 	= substr($questionMarks, 0, -2);
		$columns 		= "`" . implode('`, `', $columns) . "`";

		$stmt = $this->connection->prepare(
			"INSERT INTO `{$table}` ({$columns}) VALUES ({$questionMarks})"
		);
		if ( $stmt ) {
			$allValues = [];
			foreach ($values as $value => $type) {
				$allValues[] =& $value; // New mysqli requires params to be sent as reference.
			} 
			array_unshift($allValues, $types);
			call_user_func_array([$stmt, 'bind_param'], $allValues);
			$result = $stmt->execute();
			$stmt->close();
			return $result;			
		}
		return false;
	}
	/**
	 * Prepared statement helper to fetch values. 
	 * EDIT: I have no idea why I made this, but it was fun :D
	 * @param string 	$table 		Which table we target
	 * @param array 	$columns 	Which columns we want
	 * @param array 	$where 		Where ['value' => 'value']
	 * @param string 	$typer 		Type of $where values in order. "i" or "s". Ex. "siss"
	 * @return array 	The array containing all returned rows
	 */
	public function GetValue($table, $columns, $where, $types) {
		if ( !$table || !$columns || !$where ) return false;
		$columnString	= '`'.implode('`, `', $columns).'`';
		$columnCount 	= count($columns);
		$whereString 	= '';
		$index 			= 0;

		foreach ( $where as $item => $value ) {
			if ( $index > 0 ) $whereString .= ' AND ';
			$whereString .= "`{$item}` = ?";
			$index++;
		}
		$reftArr = ($index > 0) ? array_fill(0, $index, null) : [null]; // This is the reference array for mysqli

		$sql = "SELECT {$columnString} FROM `{$table}` WHERE {$whereString}";
		$stmt = $this->connection->prepare($sql);
		if ( $stmt ) {
			$allValues 	= [];
			$resultArr 	= [];
			$final 		= [];
			$index 		= 0;

			foreach ( $where as $key => $value ) $allValues[] =& $where[$key];
			foreach ( $columns as $column ) {
				$resultArr[] =& $reftArr[$index]; // PHP's weird reference requirement again.
				$index++;
			}
			// Let's prepend the values with the types
			array_unshift($allValues, $types);
			// And call bind_param with all those parameters.
			call_user_func_array([$stmt, 'bind_param'], $allValues);
			$stmt->execute();
			call_user_func_array([$stmt, 'bind_result'], $resultArr);

			// Let's make the result pretty.
			$index = 0;
			while ( $stmt->fetch() ) {
				$final[$index] = [];
				for ( $i=0; $i<$columnCount; $i++) {
					$final[$index][$columns[$i]] = $resultArr[$i];
				}
				$index++;
			}
			$stmt->close();
			return $final;
		}
		return false;
	}
}

class UserHandler {
	public function saveItem($prize) {
		return DB::Create()->InsertValue('prizes', ['name'], [$prize]);
	}
	public function addItemToUser($user, $prize) {
		$userHasPrize = DB::Create()->GetValue('prize_relation', ['id'], ['user' => $user, 'prize' => $prize], 'ii');
		if ( !$userHasPrize ) {
			return DB::Create()->InsertValue('prize_relation', ['user', 'prize'], [$user, $prize], 'ii');
		}
	}
	// Here is where I decided to do the rest quick & easy (& dirty)
	public function removeItemFromUser($user, $item) {
		$user = (int) $user;
		$item = (int) $item;
		$sql = "DELETE FROM `prize_relation` WHERE `user` = {$user} AND `prize` = {$item}";
		return DB::Create()->Value($sql);
	}
	public function exchange($user) {
		if ( !$user ) return false;
		// Toggle users prize between 1 & 3 (NOTE: But what if they have both?)
		$userHasSpins = DB::Create()->GetValue('prize_relation', ['id'], ['user' => $user, 'prize' => 1], 'ii'); 
		$userHasEuros = DB::Create()->GetValue('prize_relation', ['id'], ['user' => $user, 'prize' => 3], 'ii');
		// If the user had both, they are about to switch places.. (or more likely change the same one twice)
		$result1 = $result2 = null;
		if ( $userHasSpins ) $result1 = DB::Create()->Value(
			"UPDATE `prize_relation` SET `prize` = 3 WHERE `id` = {$userHasSpins[0]['id']}
		"); 
		if ( $userHasEuros ) $result2 = DB::Create()->Value(
			"UPDATE `prize_relation` SET `prize` = 1 WHERE `id` = {$userHasEuros[0]['id']}"
		); 
		if ( $result1 || $result2 ) return true;
		return false;
	}
}

$userHandler = new UserHandler();
/*
*************************************************************
* Uncomment the things below to test;
*************************************************************
*/
// var_dump($userHandler->saveItem('Cool new prize'));
// var_dump($userHandler->addItemToUser(1,3));
// var_dump($userHandler->addItemToUser(1,1));
// var_dump($userHandler->removeItemFromUser(1,1));
// var_dump($userHandler->exchange(1));

/*
*************************************************************
* About the Redis caching
*************************************************************
I don't have redis installed locally so testing would be quite a hassle, but here I might use it quite simply.
I would probably serialize all the query results into unique keys and always check if that key exists in redis
before hitting the DB. Then in every function that modifies the db I would nullify the db cache.
*/
/*
*************************************************************
* The database structure
*************************************************************
Check out the database.sql file for the structure and sample data.
*/
?>
